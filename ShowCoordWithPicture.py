'''
Created on 2014/10/30

@author: TakahiroOkada
'''
from ProjectValue import FACE_PICTURE_DIRECTORY, VERSION, RESULT_DIRECTORY, \
    VERSION_FOLDER
import numpy as np
from matplotlib import pyplot as plt, cm
import matplotlib.ticker as plticker
from matplotlib.colors import LogNorm
import os
import re
import cv2
import Image
import ProjectValue
from numpy import average
from __builtin__ import isinstance
import CommonLibrary

def show_coord_with_picture(display_size=(800, 800), is_write_rate=False, is_picture_save=False):
    files = [x for x in os.listdir(RESULT_DIRECTORY + VERSION_FOLDER) if x.endswith(".codebook")]
    
    for n, name in enumerate(files):
        print n, ":", name.split('.')[0]
    
    print "select data:" 
    try:
        file_name = files[input()].split('.')[0]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(files)
        return
    
    ss = re.split("\(|x|_|\)", file_name)
    
    global grid_size
    global string_space_width
    global face_figure_size
    global character_size_rate
    global character_margin
    
    grid_size = 140
    margin = 2
    string_space_width = 60 if is_write_rate else 0
    face_figure_size = grid_size - string_space_width
    character_size_rate = 0.7
    character_margin = 22
    
    size0 = int(ss[1])
    size1 = int(ss[2])
    global dim
    dim = int(ss[3])
    
    codebook = np.loadtxt(RESULT_DIRECTORY + VERSION_FOLDER + file_name + ".codebook", delimiter=",")
    coord = np.loadtxt(RESULT_DIRECTORY + VERSION_FOLDER + file_name + ".coord", delimiter=",")
    
    tmp = np.zeros((size0, size1, dim))
    for i in range(codebook.shape[1]):
        tmp[:, :, i] = codebook[:, i].reshape(size0, size1)
    
    view_size = (size0 * (grid_size + margin * 2), size1 * (grid_size + margin * 2))
    
    values_in_grid = CommonLibrary.get_values_in_grid((size0, size1), coord)
    
    # Open image file
    image = np.zeros((view_size[0], view_size[1], 3), np.uint8)
    image.fill(127)
    
    for i in range(size0):
        for j in range(size1):
            draw_in_grid(image, tmp[i, j, :], i * grid_size + (2 * i + 1) * margin, j * grid_size + (2 * j + 1) * margin)
    
    if(is_write_rate):
        for i in range(size0):
            for j in range(size1):
                write_rating_in_grid(image, values_in_grid[i, j, :, :], i * grid_size + (2 * i + 1) * margin, j * grid_size + (2 * j + 1) * margin)
    
    cv2.imshow("SOM result (variance, hit number)(happiness, sadness, surprise, anger, disgust, fear)", cv2.resize(image, display_size))
    cv2.waitKey(0)
    
    if(is_picture_save):
        cv2.imwrite(FACE_PICTURE_DIRECTORY + "../SOMresult.png", image)
        print "saved"
    
    pass

def draw_in_grid(img, plot_data, x_offset, y_offset):
    formed_data = []
    for i in range(dim / 2):
        x, y = plot_data[i * 2], plot_data[i * 2 + 1]
        formed_data.append((int(float(x) * face_figure_size) + x_offset, int(float(y) * face_figure_size) + y_offset))
    
    con = ProjectValue.connection
    trans = ProjectValue.INDEX_TRANSPOSE
    cv2.rectangle(img, (x_offset, y_offset), (x_offset + grid_size, y_offset + grid_size), (255, 255, 255), -1)
    for i in range(len(con[0])):
        c = [con[0][i], con[1][i]]
        start_index = [t[1] for t in trans if t[0] == c[0]]
        end_index = [t[1] for t in trans if t[0] == c[1]]
        if len(start_index) == len(end_index) == 1:
            cv2.line(img, formed_data[start_index[0]], formed_data[end_index[0]], (0, 0, 0), 1)
        pass
    pass

def write_rating_in_grid(img, rating_data, x_offset, y_offset):
    hit_number = int(rating_data[2][0])
    if(hit_number == 0):
        return
    hap, sad, sur, ang, dis, fea = rating_data[0]
    var = np.var(rating_data[1])
    font = cv2.FONT_HERSHEY_SIMPLEX
    x_align = x_offset + face_figure_size
    cv2.putText(img, "{:5.3f}".format(hap), (x_align, y_offset + 1 * character_margin), font, character_size_rate, (201, 174, 255), 1, cv2.cv.CV_AA)
    cv2.putText(img, "{:5.3f}".format(sad), (x_align, y_offset + 2 * character_margin), font, character_size_rate, (190, 146, 112), 1, cv2.cv.CV_AA)
    cv2.putText(img, "{:5.3f}".format(sur), (x_align, y_offset + 3 * character_margin), font, character_size_rate, (0, 128, 255), 1, cv2.cv.CV_AA)
    cv2.putText(img, "{:5.3f}".format(ang), (x_align, y_offset + 4 * character_margin), font, character_size_rate, (0, 0, 255), 1, cv2.cv.CV_AA)
    cv2.putText(img, "{:5.3f}".format(dis), (x_align, y_offset + 5 * character_margin), font, character_size_rate, (0, 128, 0), 1, cv2.cv.CV_AA)
    cv2.putText(img, "{:5.3f}".format(fea), (x_align, y_offset + 6 * character_margin), font, character_size_rate, (0, 0, 0), 1, cv2.cv.CV_AA)
    if(hit_number > 1):
        cv2.putText(img, "{:6.5f}".format(var).lstrip('0'), (x_offset, y_offset + face_figure_size + character_margin), font, character_size_rate, (0, 200, 200), 1, cv2.cv.CV_AA)
    cv2.putText(img, "{:2d}".format(hit_number), (x_offset, y_offset + face_figure_size + 2 * character_margin), font, character_size_rate, (0, 0, 0), 1, cv2.cv.CV_AA)
    pass
