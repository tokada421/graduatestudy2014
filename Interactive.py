'''
Created on 2014/10/08

@author: TakahiroOkada
'''

import cv2
import os
import ProjectValue

def draw(image, pos, visi, con):
    # for i in range(len(con[0])):
        # print con[0][i], con[1][i]
        # if(visi[con[0][i]] == 0 or visi[con[1][i]] == 0):
            # continue
        # p1 = (pos[con[0][i]], pos[con[0][i] + n])
        # p2 = (pos[con[1][i]], pos[con[1][i] + n])
        # cv2.line(image, p1, p2, (255, 0, 0), 3)
    
    for i in range(len(pos)):
        if(visi[i] == 0):
            continue
        cv2.circle(image, pos[i], 3, (0, 0, 255), -3)

def interactive():
    FACE_PICTURE_DIRECTORY = ProjectValue.FACE_PICTURE_DIRECTORY
    
    tracker = ProjectValue.tracker
    connection = ProjectValue.connection
    
    use_point_list = range(0, 65 + 1) # ProjectValue.USE_POINT_LIST
    
    files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY) if os.path.isfile(FACE_PICTURE_DIRECTORY + x) and not x.endswith(".dat")]
    
    # usePointList = range(0, 65+1)
    for n, name in enumerate(files):
        print n, ":", name
    
    print "select picture:"  
    try:
        fileName = files[input()]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(files)
        return
    
    img = cv2.imread(FACE_PICTURE_DIRECTORY + fileName)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    tracker.setWindowSizes((11, 9, 7))
    tracker.frame_skip = -1
    tracker.iterations = 25
    tracker.clamp = 4.0
    tracker.tolerance = 1.0
    
    b = tracker.update(gray)
    if(tracker.update(gray)):
        shape, visi = tracker.get2DShape()
        
        
        # filtering
        n = len(shape) / 2
        position = zip(shape[:n], shape[n:])
        
        position = [x for i, x in enumerate(position) if i in use_point_list]
        
        draw(img, position, visi, connection)
        
        cv2.imshow(fileName , img)
        cv2.waitKey(0)
    
    cv2.destroyAllWindows()