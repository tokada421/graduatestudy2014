'''
Created on 2014/10/22

@author: TakahiroOkada
'''
from ProjectValue import FACE_PICTURE_DIRECTORY, USE_POINT_LIST, VERSION, \
    RESULT_DIRECTORY, VERSION_FOLDER
from matplotlib import pyplot as plt, cm
import os
import re
import numpy as np
from matplotlib.colors import LogNorm
import cv2
import ProjectValue

def view_codebook_coord(shows_codebook=True, shows_coord=True):
    files = [x for x in os.listdir(RESULT_DIRECTORY + VERSION_FOLDER) if x.endswith(".codebook")]
    
    for n, name in enumerate(files):
        print n, ":", name.split('.')[0]
    
    print "select data:" 
    try:
        file_name = files[input()].split('.')[0]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(files)
        return
    
    ss = re.split("\(|x|_|\)", file_name)
    
    global size0
    global size1
    global dim
    size0 = int(ss[1])
    size1 = int(ss[2])
    dim = int(ss[3])
    
    codebook = np.loadtxt(RESULT_DIRECTORY + VERSION_FOLDER + file_name + ".codebook", delimiter=",")
    
    global sH
    global sV
    sH, sV = 16, 16
    
    global no_row_in_plot
    global no_col_in_plot
    no_row_in_plot = dim / 6 + 1  # 6 is arbitrarily selected
    if no_row_in_plot <= 1:
        no_col_in_plot = dim
    else:
        no_col_in_plot = 6
    
    global tmp
    tmp = np.zeros((size0, size1, dim))
    for i in range(codebook.shape[1]):
        tmp[:, :, i] = codebook[:, i].reshape(size0, size1)
    
    if(shows_codebook):
        show_codebook()
    
    global coord
    coord = np.loadtxt(RESULT_DIRECTORY + VERSION_FOLDER + file_name + ".coord", delimiter=",")
    
    if(shows_coord):
        show_coord()
    
    pass

def show_codebook():
    plt.figure(figsize=(sH, sV))
    
    axisNum = 0
    while axisNum < dim:
        axisNum += 1
        ax = plt.subplot(no_row_in_plot, no_col_in_plot, axisNum)
        index = axisNum - 1
        mp = tmp[:, :, index]  # codebook[:, index].reshape(size0, size1)
        pl = plt.pcolor(mp[::-1])
        plt.title("Variable-" + str(USE_POINT_LIST[index / 2]) + ("x" if index % 2 == 0 else "y"))
        text_size = 2.8
        font = {'size' : text_size * sH / no_col_in_plot}
        plt.rc('font', **font)
        plt.axis('off')
        plt.axis([0, size0, 0, size1])
        ax.set_yticklabels([])
        ax.set_xticklabels([])
        plt.colorbar(pl)
        print "dim" , axisNum, "is passed"
    
    print "codebook plot finished"
    plt.show()

def show_coord():
    fig = plt.figure(figsize=(size1 / 2, size0 / 2))
    ax = fig.add_subplot(111)
    ax.xaxis.set_ticks([i for i in range(0, size1)])
    ax.yaxis.set_ticks([i for i in range(0, size0)])
    ax.xaxis.set_ticklabels([])
    ax.yaxis.set_ticklabels([])
    ax.grid(True, linestyle='-', linewidth=.5)
    a = plt.hist2d(coord[:, 1], coord[:, 0], bins=(size1, size0), alpha=.0, norm=LogNorm(), cmap=cm.jet)
    x = np.arange(.5, size1 + .5, 1)
    y = np.arange(.5, size0 + .5, 1)
    X, Y = np.meshgrid(x, y)
    area = a[0].T * 12
    plt.scatter(X, Y, s=area, alpha=0.2, c='b', marker='o', cmap='jet', linewidths=3, edgecolor='r')
    plt.scatter(X, Y, s=area, alpha=0.9, c='None', marker='o', cmap='jet', linewidths=3, edgecolor='r')
    plt.xlim(0, size1)
    plt.ylim(0, size0)
    
    fig.canvas.mpl_connect('button_press_event', onclick)
    
    print "coord plot finished"
    plt.show()

def onclick(event):
    if isinstance(event.xdata, type(None)) or isinstance(event.ydata, type(None)):
        return
    
    plot_data = tmp[event.xdata, event.ydata, :]
    size = 600
    
    formed_data = []
    for i in range(dim / 2):
        x, y = plot_data[i * 2], plot_data[i * 2 + 1]
        formed_data.append((int(float(x) * size), int(float(y) * size)))
    
    img = np.zeros((size, size, 3), np.uint8)
    
    # line connection setting
    con = ProjectValue.connection
    trans = ProjectValue.INDEX_TRANSPOSE
    for i in range(len(con[0])):
        c = [con[0][i], con[1][i]]
        start_index = [t[1] for t in trans if t[0] == c[0]]
        end_index = [t[1] for t in trans if t[0] == c[1]]
        if len(start_index) == len(end_index) == 1:
            cv2.line(img, formed_data[start_index[0]], formed_data[end_index[0]], (0, 255, 255), 1)
        pass
     
    for i in range(len(formed_data)):
        cv2.circle(img, formed_data[i], 3, (0, 0, 255), -3)
        pass
    
    # cv2.imshow("Plot at (" + str(event.xdata) + "," + str(event.ydata) + ")" , img)
    cv2.imshow("Plot" , img)
