'''
Created on 2014/10/08

@author: TakahiroOkada
'''
import facetracker

# select version
VERSION = 2

if(not (0 <= VERSION <= 2)):
    raise "version error"

BASE_DIRECTORY = r"D:\research"'\\'
VERSION_FOLDER = "ver" + str(VERSION) + "\\"
FACE_PICTURE_DIRECTORY =  BASE_DIRECTORY + "Faces\\"
DATA_DIRECTORY = BASE_DIRECTORY + "Data\\"
NORMALIZED_DIRECTORY = FACE_PICTURE_DIRECTORY + "normalized\\"
RESULT_DIRECTORY = FACE_PICTURE_DIRECTORY + "result\\"

tracker = facetracker.FaceTracker(DATA_DIRECTORY + 'face2.tracker')  # @UndefinedVariable
connection = facetracker.LoadCon(DATA_DIRECTORY + 'face.con')  # @UndefinedVariable

USE_POINT_LIST_V1 = (range(17, 21 + 1) +  # right eyebrow
                     range(22, 26 + 1) +  # left eyebrow
                     range(36, 41 + 1) +  # right eye
                     range(42, 47 + 1) +  # left eye
                     range(48, 59 + 1) +  # outside of mouse
                     range(60, 65 + 1))   # inside of mouse

USE_POINT_LIST_V2 = (range(17, 21 + 1) +  # right eyebrow
                     range(22, 26 + 1) +  # left eyebrow
                     range(27, 30 + 1) +
                     range(36, 41 + 1) +  # right eye
                     range(42, 47 + 1) +  # left eye
                     range(60, 65 + 1))   # inside of mouse

#if VERSION == 0
USE_POINT_LIST = range(0, 66) # all
if(VERSION == 1):
    USE_POINT_LIST = USE_POINT_LIST_V1
elif(VERSION == 2):
    USE_POINT_LIST = USE_POINT_LIST_V2

INDEX_TRANSPOSE = []
for i in range(len(USE_POINT_LIST)):
    INDEX_TRANSPOSE.append([USE_POINT_LIST[i], i])

RATE_COLUMN_NUMBER = 6
'''
def contains(a_list, a_filter):
    for x in a_list:
        if a_filter(x):
            return True
    return False
'''