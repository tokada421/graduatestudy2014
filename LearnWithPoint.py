'''
Created on 2014/10/16

@author: TakahiroOkada
'''
import Sompy
import os
from ProjectValue import FACE_PICTURE_DIRECTORY, USE_POINT_LIST, VERSION, \
    NORMALIZED_DIRECTORY, RESULT_DIRECTORY, VERSION_FOLDER
import numpy as np
from datetime import datetime
from sklearn import neighbors

def learn_with_point(size=(10, 10)):
    size0 = size[0]
    size1 = size[1]
    dim = len(USE_POINT_LIST) * 2
    
    files = [x for x in os.listdir(NORMALIZED_DIRECTORY + VERSION_FOLDER)]
    data_list = []
    for a_file in files:
        data = []
        [data.extend(row) for row in np.loadtxt(NORMALIZED_DIRECTORY + VERSION_FOLDER + a_file, delimiter=",")]
        data_list.append(data)
    
    learning_data_list = np.vstack(data_list)
    
    som = Sompy.SOM('learn_with_point', learning_data_list, mapsize=[size0, size1], norm_method=None, initmethod='pca')
    som.train(n_job=2, shared_memory='no')
    
    dt = datetime.now().strftime('%Y%m%d-%H%M%S')
    file_name_base = RESULT_DIRECTORY + VERSION_FOLDER + dt + "(" + str(size0) + "x" + str(size1) + "_" + str(dim) + ")"
    
    # save codebook
    codebook = getattr(som, 'codebook')  # 2D array [size*size][dim]
    np.savetxt(file_name_base + ".codebook", codebook, delimiter=",")
    
    # save hitmap
    data_tr = getattr(som, "data_raw")
    proj = project_data(codebook , data_tr)
    coord = som.ind_to_xy(proj)
    np.savetxt(file_name_base + ".coord", coord, delimiter=",")
    
    print "Data saved"
    
    pass

def project_data(codebook, data):
    clf = neighbors.KNeighborsClassifier(n_neighbors=1)
    labels = np.arange(0, codebook.shape[0])
    clf.fit(codebook, labels)
    return clf.predict(data)

'''
import Sompy
import os
from ProjectValue import FACE_PICTURE_DIRECTORY, USE_POINT_LIST
import numpy as np
from datetime import datetime
from sklearn import neighbors

def learn_with_point(size0 = 40, size1 = 40):
    dim = len(USE_POINT_LIST) * 2
    
    files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY + "normalized\\")]
    data_list = []
    for a_file in files:
        data = []
        with open(FACE_PICTURE_DIRECTORY + "normalized\\" + a_file, "r") as f:
            for line in f.readlines():
                data.extend([float(x) for x in line.split(',')])
                pass
            pass
        data_list.append(data)
        pass
    
    learning_data_list = np.vstack(data_list)
    
    som = Sompy.SOM('learn_with_point', learning_data_list, mapsize=[size0, size1], norm_method=None, initmethod='pca')
    som.train(n_job=2, shared_memory='no')
    
    dt = datetime.now().strftime('%Y%m%d-%H%M%S')
    file_name_base = FACE_PICTURE_DIRECTORY + "result\\" + dt + "(" + str(size0) + "x" + str(size1) + "_" + str(dim) + ")"
    
    # save codebook
    codebook = getattr(som, 'codebook')  # 2D array [size*size][dim]
    np.savetxt(file_name_base + ".codebook", codebook, delimiter=",")
    
    # save hitmap
    data_tr = getattr(som, "data_raw")
    proj = project_data(codebook , data_tr)
    coord = som.ind_to_xy(proj)
    np.savetxt(file_name_base + ".coord", coord, delimiter=",")
    
    print "Data saved"
    
    pass

def project_data(codebook, data):
    clf = neighbors.KNeighborsClassifier(n_neighbors=1)
    labels = np.arange(0, codebook.shape[0])
    clf.fit(codebook, labels)
    return clf.predict(data)
'''
