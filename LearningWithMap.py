'''
Created on 2014/12/09

@author: TakahiroOkada
'''

import os
import re
import numpy as np
from ProjectValue import FACE_PICTURE_DIRECTORY, VERSION, RESULT_DIRECTORY, \
    VERSION_FOLDER
from QLearningSom import QLearning, Superviser
import CommonLibrary
import time
from multiprocessing.process import Process

def learning_with_map():
    files = [x for x in os.listdir(RESULT_DIRECTORY + VERSION_FOLDER) if x.endswith(".codebook")]
    
    for n, name in enumerate(files):
        print n, ":", name.split('.')[0]
    
    print "select data:" 
    try:
        file_name = files[input()].split('.')[0]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(files)
        return
    
    ss = re.split("\(|x|_|\)", file_name)
    
    size0 = int(ss[1])
    size1 = int(ss[2])
    dim = int(ss[3])
    
    codebook = np.loadtxt(RESULT_DIRECTORY + VERSION_FOLDER + file_name + ".codebook", delimiter=",")
    coord = np.loadtxt(RESULT_DIRECTORY + VERSION_FOLDER + file_name + ".coord", delimiter=",")
    
    tmp = np.zeros((size0, size1, dim))
    for i in range(codebook.shape[1]):
        tmp[:, :, i] = codebook[:, i].reshape(size0, size1)
        
    division = 10
    
    if not os.path.exists(RESULT_DIRECTORY + VERSION_FOLDER + file_name):
        os.mkdir(RESULT_DIRECTORY + VERSION_FOLDER + file_name)
    
    s = Superviser(CommonLibrary.get_values_in_grid((size0, size1), coord), epsilon=0.015)
    n_count = 5000000
    '''
    print "### start @" + file_name + " in " + str(n_count) + "counts ###"
    start = time.time()
    for i in range(5, 9):
    # for i in range(division + 1):
        jobs = []
        for j in range(5, 9):
        # for j in range(division + 1):
            # for k in range(division):
            alpha = (float(i) / division)
            gamma = (float(j) / division)
            epsilon = 0.1
            tau = 0.4  # (float(k) / (division - 1))
            pro = QL_Process(QLearning(tmp, s), n_count, alpha, gamma, epsilon, tau, file_name)
            # sql = QLearning(tmp, s)
            # sql.learn(10000, alpha=(float(i) / (division - 1)), gamma=(float(j) / (division - 1)), epsilon=0.1, tau=0.5)
            # sql.dump_q_value_to_file(file_name)
            jobs.append(pro)
        for p in jobs:
            p.start()
        
        for p in jobs:
            p.join()
        pass
    pass
    end = time.time()
    seconds = int(end - start)
    print "### all finished in {0}h {1}m {2}s ###".format(seconds / 60 / 60, seconds / 60 % 60, seconds % 60)
    '''
    sql = QLearning(tmp, s)
    sql.learn(n_count, alpha=0.6, gamma=0.6, epsilon=0.4, tau=0.8)
    # sql.dump_q_value()
    # sql.dump_q_value_with_threshold(0.8
    sql.dump_q_value_to_file(file_name)
    # state
    # action
    # reward

class QL_Process(Process):
    def __init__(self, sql, n_count, alpha, gamma, epsilon, tau, som_filename):
        super(QL_Process, self).__init__()
        self.learner = sql
        self.n_count = n_count
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        self.tau = tau
        self.som_filename = som_filename
        self.id = QL_Process.id
        QL_Process.id += 1
    
    def run(self):
        print "# start ", str(self.id)
        print ""
        self.learner.learn(self.n_count, self.alpha, self.gamma, self.epsilon, self.tau)
        print "# end ", str(self.id)
        self.learner.dump_q_value_to_file(self.som_filename)
        print ""
    
    id = 0
#     s = Superviser(CommonLibrary.get_values_in_grid((size0, size1), coord), epsilon=0.1)
#     sql = QLearning(tmp, s)
#     sql.learn(1000, alpha=0.2, gamma=0.9, epsilon=0.2, tau=0.5)
#     # sql.dump_q_value()
#     sql.dump_q_value_with_threshold(0.95)
#        ||
#       \  /
#        \/
# ### Dump of Q value with over 0.95
# (4, 7) (0, 0, 0, 0, 0, 1) 3.44387549509
# (3, 8) (0, 0, 0, 0, 0, 1) 3.30448157055
# (7, 7) (1, 1, 0, 0, 0, 0) 2.24081033761
# (2, 1) (0, 1, 0, 1, 0, 0) 2.3945698344
# (5, 1) (1, 0, 0, 1, 1, 0) 1.86375322941
# (2, 5) (1, 1, 1, 0, 0, 1) 1.92860707158
# (6, 3) (1, 1, 0, 1, 1, 0) 0.956019534889
# (6, 7) (0, 1, 1, 1, 0, 0) 1.5991355747
# (3, 0) (0, 1, 0, 1, 0, 0) 2.46476621339
# (2, 2) (0, 0, 0, 1, 0, 0) 0.98643419095
# (8, 6) (1, 1, 0, 0, 0, 0) 2.14114490758
# (1, 4) (1, 1, 1, 0, 0, 1) 2.0473328713
# (0, 7) (0, 0, 0, 1, 0, 0) 1.58270033631
# (0, 8) (0, 0, 0, 1, 0, 0) 1.45890551778
# (6, 1) (1, 0, 1, 1, 1, 0) 0.950535401397
# (3, 1) (0, 1, 0, 1, 0, 0) 2.59177526258
# (5, 0) (1, 0, 0, 1, 1, 0) 1.55615016864
# (7, 8) (0, 1, 1, 1, 0, 0) 1.43363406393

#     s = Superviser(CommonLibrary.get_values_in_grid((size0, size1), coord), epsilon=0.1)
#     sql = QLearning(tmp, s)
#     sql.learn(1000, alpha=0.9, gamma=0.2, epsilon=0.2, tau=0.5)
#     # sql.dump_q_value()
#     sql.dump_q_value_with_threshold(0.95)
# ### Dump of Q value with over 0.95
# (7, 3) (1, 0, 0, 0, 0, 0) 0.9999
# (1, 3) (0, 0, 0, 1, 1, 1) 0.957510751805
# (6, 6) (0, 0, 0, 0, 0, 1) 0.963098194543
# (5, 4) (0, 0, 0, 0, 0, 1) 0.963090697116
# (7, 2) (0, 0, 0, 0, 0, 0) 1.10971587386
# (3, 2) (0, 0, 0, 0, 1, 1) 0.953172509597
# (2, 7) (0, 0, 0, 1, 0, 0) 1.01389918828
# (0, 7) (0, 0, 0, 0, 0, 0) 0.9999
# (3, 1) (0, 1, 0, 1, 1, 0) 0.96853949874
# (7, 4) (0, 1, 0, 0, 0, 0) 0.996706799998
# (5, 0) (0, 0, 0, 0, 0, 0) 0.999999999

