'''
Created on 2014/12/17

@author: TakahiroOkada
'''

import os
import re
import numpy as np
from ProjectValue import FACE_PICTURE_DIRECTORY, VERSION, RATE_COLUMN_NUMBER
import CommonLibrary
from sklearn.svm.classes import NuSVR

def supervised_learning():
    files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY + "result\\ver" + str(VERSION) + "\\") if x.endswith(".codebook")]
    
    for n, name in enumerate(files):
        print n, ":", name.split('.')[0]
    
    print "select data:" 
    try:
        file_name = files[input()].split('.')[0]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(files)
        return
    
    ss = re.split("\(|x|_|\)", file_name)
    
    size0 = int(ss[1])
    size1 = int(ss[2])
    dim = int(ss[3])
    
    codebook = np.loadtxt(FACE_PICTURE_DIRECTORY + "result\\ver" + str(VERSION) + "\\" + file_name + ".codebook", delimiter=",")
    coord = np.loadtxt(FACE_PICTURE_DIRECTORY + "result\\ver" + str(VERSION) + "\\" + file_name + ".coord", delimiter=",")
    
    values_in_grid = CommonLibrary.get_values_in_grid((size0, size1), coord)
    
    vig = values_in_grid.reshape((size0 * size1, 3, 6))
    
    tmp = np.zeros((vig.shape[0], RATE_COLUMN_NUMBER))
    for i in range(vig.shape[0]):
        tmp[i, :] = vig[i, 0, :]
    
    svrm = SVRMachine()
    svrm.fit(codebook, tmp)
    print svrm.predict(codebook)
    pass

class SVRMachine(object):
    def __init__(self):
        self.array = []
        for _ in range(RATE_COLUMN_NUMBER):
            self.array.append(NuSVR())
        pass
    
    def fit(self, X, ys):
        for i in range(RATE_COLUMN_NUMBER):
            self.array[i].fit(X, ys[:, i])
        pass
    
    def predict(self, X):
        return np.array([svr.predict(X) for svr in self.array]).T
