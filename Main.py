'''
Created on 2014/10/08

@author: TakahiroOkada
'''
from MakeProt import make_prot
from PlotNormalize import plot_normalize
from LearnWithPoint import learn_with_point
from ViewCodebook import view_codebook_coord
from ShowCoordWithPicture import show_coord_with_picture
from LearningWithMap import learning_with_map

import numpy as np
import Sompy
from matplotlib import pyplot as plt
import sys
import ProjectValue
import os
import cv2

from ProjectValue import FACE_PICTURE_DIRECTORY

from Interactive import interactive
from PlotCompare import plot_compare
from PlotAll import plot_all
import QLearningTest
from SupervisedLearning import supervised_learning
from ViewQValue import view_q_value
from MakeQValueGraph import make_q_value_graph
from ShowRatingMap import show_rating_map

def SOM_learning():
    msz0 = 20
    msz1 = 20
    cd = msz0 * msz1 * 1 * 1
    dlen = 4000  # 100 * 1000 * 1 * 1 * 1  # +224
    dim = 3
    Data = np.random.random((dlen, dim))  # np.random.randint(0, 2, size=(dlen, dim))
    
    # reload(sys.modules['Sompy'])
    sm = Sompy.SOM('color-map', Data, mapsize=[msz0, msz1], norm_method='var', initmethod='pca')
    
    sm.train(n_job=2, shared_memory='no')
    
    tmp = np.zeros((msz0, msz1, dim))
    codebook = getattr(sm, 'codebook')
    codebook = Sompy.denormalize_by(Data, codebook)
    for i in range (codebook.shape[1]):
        tmp[:, :, i] = codebook[:, i].reshape(msz0, msz1)
    # np.savetxt(FACE_PICTURE_DIRECTORY+"..\\test.csv", codebook, delimiter=",")
    fig = plt.imshow(tmp[:, :, 0:3])
    fig.set_interpolation('nearest')
    plt.show()
    # sm.view_map(which_dim='all')
    # sm.hit_map()
    pass

def reinforce_learning():
    reward = np.zeros((3, 3))
    reward[1, 1] = -5
    reward[1, 2] = -5
    reward[2, 2] = 10
    
    ql = QLearningTest.QLearning(reward)
    ql.learn(100, 0.2, 0.9, 0.2)
    ql.dump()
    ql.routing((2, 2))
    pass

if __name__ == '__main__':  # TODO argument
    # make_prot()
    
    # plot_normalize()
    
    # learn_with_point(size=(15, 15))
    
    
    # view_codebook_coord(shows_codebook=False, shows_coord=True)
    # show_coord_with_picture(display_size=(1000, 1000), is_write_rate=False, is_picture_save=True)
    
    learning_with_map()
    
    # view_q_value()
    
    # make_q_value_graph()
    
    # show_rating_map()
    
    ''' methods of test '''
    # --------------
    # interactive()
    # plot_compare()
    
    # plot_all()
    
    # SOM_learning()
    
    # reinforce_learning()
        
    # supervised_learning()
    #---------------
    
    print "process finished"
