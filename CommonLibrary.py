'''
Created on 2014/12/17

@author: TakahiroOkada
'''

import numpy as np
import os
from ProjectValue import FACE_PICTURE_DIRECTORY, VERSION, RATE_COLUMN_NUMBER

def check_range(x, y, state_size):
    if(x >= 0 and x < state_size[0] and y >= 0 and y < state_size[1]):
        return True
    else:
        return False

def get_values_in_grid(sizes, coord, is_lerped=True):
    size0 = sizes[0]
    size1 = sizes[1]
    
    # coordbook meanvalue
    rating_values = np.loadtxt(FACE_PICTURE_DIRECTORY + "RatingValues.txt", skiprows=1, usecols=(1, 2, 3, 4, 5, 6))
    valid_numbers = [int(x.split(".")[3]) for x in os.listdir(FACE_PICTURE_DIRECTORY + "normalized\\ver" + str(VERSION) + "\\")]
    values_in_coord = [(int(coord[x][2]), rating_values[valid_numbers[x] - 1]) for x in range(len(valid_numbers))]
    values_in_array_stat = []
    for i in range(size0 * size1):
        data = np.array([x[1] for x in values_in_coord if x[0] == i])
        
        average = np.average(data, axis=0)
        if(not isinstance(average, np.ndarray)):
            average = np.zeros((RATE_COLUMN_NUMBER))
        variance = np.var(data, axis=0)
        if(not isinstance(variance, np.ndarray)):
            variance = np.zeros((RATE_COLUMN_NUMBER))
        hit_number = np.zeros((RATE_COLUMN_NUMBER))
        hit_number.fill(data.shape[0])
        
        values_in_array_stat.append((average, variance, hit_number))
    
    raw_data = np.array(values_in_array_stat).reshape((size0, size1, 3, RATE_COLUMN_NUMBER))
    
    if not is_lerped:
        return raw_data
    
    result_data = np.zeros(raw_data.shape)
    
    for i in range(raw_data.shape[0]):
        for j in range(raw_data.shape[1]):
            if raw_data[i, j, 2, 0] == 0.0:
                d = []
                if i > 0:
                    a_grid = raw_data[i - 1, j, :, :]
                    if a_grid[2, 0] != 0.0:
                        d.append(a_grid[0, :])
                if i < raw_data.shape[0] - 1:
                    a_grid = raw_data[i + 1, j, :, :]
                    if a_grid[2, 0] != 0.0:
                        d.append(a_grid[0, :])
                if j > 0:
                    a_grid = raw_data[i, j - 1, :, :]
                    if a_grid[2, 0] != 0.0:
                        d.append(a_grid[0, :])
                if j < raw_data.shape[1] - 1:
                    a_grid = raw_data[i, j + 1, :, :]
                    if a_grid[2, 0] != 0.0:
                        d.append(a_grid[0, :])
                new_grid = np.zeros((3, RATE_COLUMN_NUMBER))
                new_grid[0, :] = np.average(np.array(d), axis=0)
                new_grid[1, :].fill(-1)
                new_grid[2, :].fill(-1)
                result_data[i, j, :, :] = new_grid
            else:
                result_data[i, j, :, :] = raw_data[i, j, :, :]
            pass
        pass
    
    return result_data

def string_to_tuple(tuple_string):
    return tuple([int(x) for x in tuple_string[1:-1].split(',')])
