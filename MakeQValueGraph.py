'''
Created on 2014/12/24

@author: TakahiroOkada
'''

import os
from ProjectValue import RESULT_DIRECTORY, VERSION_FOLDER
from CommonLibrary import string_to_tuple
from QLearningSom import QLearning
import numpy as np
import re
from numpy import average
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm

def make_q_value_graph():
    dirs = [x for x in os.listdir(RESULT_DIRECTORY + VERSION_FOLDER) if os.path.isdir(RESULT_DIRECTORY + VERSION_FOLDER + x)]
    
    for n, name in enumerate(dirs):
        print n, ":", name
    
    print "select one_data:" 
    try:
        dir_name = dirs[input()]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(dirs)
        return
    
    files = [y for y in os.listdir(RESULT_DIRECTORY + VERSION_FOLDER + dir_name + "\\") if y.endswith(".qval")]
    
    for n, name in enumerate(files):
        print n, ":", name
    
    try:
        print "start index?"
        a = int(input())
        if not 0 <= a < len(files):
            print "Out of index"
            return
        print "end index?"
        b = int(input())
        if not 0 <= b < len(files):
            print "Out of index"
            return
        if a > b:
            print "Invalid range"
            return
        data_range = range(a, b + 1)
    except SyntaxError:
        print "Syntax Error"
        return
    except NameError:
        print "required number"
        return
    
    x = np.arange(0, 1.0625, 0.1)
    y = np.arange(0, 1.0625, 0.1)
    X, Y = np.meshgrid(x, y)
    Z_q_max = np.zeros(X.shape)
    Z_q_max_avg = np.zeros(X.shape)
    Z_q_min_avg = np.zeros(X.shape)
    # Z_one = np.ones(X.shape)
    plt.show()
    
    plot_data = {}
    for i in data_range:
        file_name = files[i]
        one_data = []
        with open(RESULT_DIRECTORY + VERSION_FOLDER + dir_name + "\\" + file_name, "r") as f:
            line = f.readline()
            while line:
                elem = line.split('|')
                state = string_to_tuple(elem[0])
                action = string_to_tuple(elem[1])
                q_value = float(elem[2])
                row = QValueData(state, action, q_value)
                one_data.append(row)
                
                line = f.readline()
            pass
        one_data.sort(key=lambda x: x.q_value, reverse=True)
        elem = file_name.split('_')
        alpha = float(elem[1])
        gamma = float(elem[2])
        max_avg = average([x.q_value for x in one_data[:100]])
        max_value = one_data[0].q_value
        min_avg = average([x.q_value for x in one_data[-100:]])
        plot_data.setdefault(alpha, {})
        plot_data[alpha][gamma] = (max_value, max_avg, min_avg)
    
    for i in range(11):
        if i == 0:
            continue
        x_index = i / 10.0
        for j in range(11):
            y_index = j / 10.0
            try:
                value = plot_data[y_index][x_index]
                max_value = value[0]
                max_avg = value[1]
                min_avg = value[2]
                
                if max_value > 1.0:
                    max_value = -0.5
                    max_avg = -0.5
                    min_avg = -0.5
                
            except KeyError:
                max_value = -1.0
                max_avg = -1.0
                min_avg = -1.0
            Z_q_max[i, j] = max_value
            Z_q_max_avg[i, j] = max_avg
            Z_q_min_avg[i, j] = min_avg
    
    print "aggregate finished"
    
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot_wireframe(X, Y, Z_q_max, color="red")
    ax.plot_wireframe(X, Y, Z_q_max_avg, color="blue")
    ax.plot_wireframe(X, Y, Z_q_min_avg, color="green")
    ax.set_xlabel("alpha")
    ax.set_ylabel("gamma")
    plt.show()

class QValueData(object):
    def __init__(self, state, action, q_value):
        self.state = state
        self.action = action
        self.q_value = q_value
