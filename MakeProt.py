'''
Created on 2014/10/08

@author: TakahiroOkada
'''

# import facetracker
import cv2
import os
import ProjectValue
import numpy as np

def make_prot():
	FACE_PICTURE_DIRECTORY = ProjectValue.FACE_PICTURE_DIRECTORY
	
	tracker = ProjectValue.tracker
	# connection = ProjectValue.connection
	
	tracker.setWindowSizes((11, 9, 7))
	tracker.frame_skip = -1
	tracker.iterations = 25
	tracker.clamp = 4.0
	tracker.tolerance = 1.0
	
	files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY) if os.path.isfile(FACE_PICTURE_DIRECTORY + x) and not x.endswith(".dat")]
	
	for n, name in enumerate(files):
		print n, ":", name
	
		img = cv2.imread(FACE_PICTURE_DIRECTORY + name)
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		
		tracker.update(gray)
		if(tracker.update(gray)):
			shape, _ = tracker.get2DShape()
			
			# fitting
			num = len(shape) / 2
			position = zip(shape[:num], shape[num:])
			
			# output
			np.savetxt(FACE_PICTURE_DIRECTORY + name + ".dat", position, delimiter=",")
			
			print n, ":", name, " is finished."
			print
			
		else:
			print "caution"
			pass
	
'''
# import facetracker
import cv2
import os
import ProjectValue
# import np as np

def make_prot():
	FACE_PICTURE_DIRECTORY = ProjectValue.FACE_PICTURE_DIRECTORY
	
	tracker = ProjectValue.tracker
	# connection = ProjectValue.connection
	
	tracker.setWindowSizes((11, 9, 7))
	tracker.frame_skip = -1
	tracker.iterations = 25
	tracker.clamp = 4.0
	tracker.tolerance = 1.0
	
	use_point_list = ProjectValue.USE_POINT_LIST
	
	files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY) if os.path.isfile(FACE_PICTURE_DIRECTORY + x) and not x.endswith(".dat")]
	
	for n, name in enumerate(files):
		print n, ":", name
	
		img = cv2.imread(FACE_PICTURE_DIRECTORY + name)
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		
		tracker.update(gray)
		if(tracker.update(gray)):
			shape, _ = tracker.get2DShape()
			
			# filtering
			num = len(shape) / 2
			buf = zip(shape[:num], shape[num:])
			position = [(i, x) for i, x in enumerate(buf) if i in use_point_list]
			# test codes
			# test_position = [(i,x) for i, x in enumerate(buf) if i in usePointList]
			# print [x for x in zip(position)]
			# print position[0][0][0]
			# print "===================================="
			# print test_position[0][1][0][0]
			# x = position[0] #sample
			# print '{0}:({1},{2})'.format(0, x[0][0], x[1][0])
			# y = test_position[0] #sample
			# print '{0}:({1},{2})'.format(y[0], y[1][0][0], y[1][1][0])
			# print position[0][0][0], position[0][1][0]
			
			# output
			with open(FACE_PICTURE_DIRECTORY + name + ".dat", "w") as f:
				height, width, _ = img.shape
				f.write('{0},{1},WxH\n'.format(width, height))
				f.write('\n'.join('{0},{1},{2}'.format(x[0], x[1][0][0], x[1][1][0]) for x in position))
				print n, ":", name, " is finished."
			
			print
		else:
			print "caution"
			pass
'''