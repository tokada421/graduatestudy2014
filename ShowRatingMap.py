'''
Created on 2015/01/08

@author: TakahiroOkada
'''
from CommonLibrary import get_values_in_grid
from ProjectValue import RESULT_DIRECTORY, VERSION_FOLDER, RATE_COLUMN_NUMBER
import os
import re
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
from matplotlib import cm

def show_rating_map():
    files = [x for x in os.listdir(RESULT_DIRECTORY + VERSION_FOLDER) if x.endswith(".codebook")]
    
    for n, name in enumerate(files):
        print n, ":", name.split('.')[0]
    
    print "select data:" 
    try:
        file_name = files[input()].split('.')[0]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(files)
        return
    
    ss = re.split("\(|x|_|\)", file_name)
    
    size0 = int(ss[1])
    size1 = int(ss[2])
    
    coord = np.loadtxt(RESULT_DIRECTORY + VERSION_FOLDER + file_name + ".coord", delimiter=",")
    
    value_grid = get_values_in_grid((size0, size1), coord, is_lerped=True)
    
    x = np.arange(0, size0)
    y = np.arange(0, size1)
    X, Y = np.meshgrid(x, y)
    Zs = np.zeros((size0, size1, RATE_COLUMN_NUMBER))
    
    for i in x:
        for j in y:
            Zs[i, j, :] = value_grid[i, j, 0, :]
            pass
        pass
    
    labels = ["Happiness", "Sadness", "Surprise", "Anger", "Disgust", "Fear"]
    
    for n, label in enumerate(labels):
        print n, ":", label
    while True:
        try:
            fig = plt.figure()
            ax = Axes3D(fig)
            index = int(input())
            fig.canvas.set_window_title(labels[index])
            ax.plot_surface(X, Y, Zs[:, :, index], rstride=1, cstride=1, antialiased=False)
            ax.set_zlim(1.0, 5.0)
            ax.set_xlabel("vertical")
            ax.set_ylabel("horizontal")
            ax.set_title(labels[index])
            # fig.colorbar(surf, shrink=0.5, aspect=5)
            plt.show()
            matplotlib.pyplot.close()
        except NameError:
            print "required number"
            return
        except IndexError:
            print "required 0 to", RATE_COLUMN_NUMBER
            return
        pass
    pass
    '''
    for i in range(RATE_COLUMN_NUMBER):
        fig = plt.figure()
        ax = Axes3D(fig)
        surf = ax.plot_surface(X, Y, Zs[:, :, i], rstride=1, cstride=1, antialiased=False)
        ax.set_zlim(0.0, 5.0)
        ax.set_xlabel("vertical")
        ax.set_ylabel("horizontal")
        ax.set_title(i)
        # fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.show()
        matplotlib.pyplot.close()
        
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot_wireframe(X, Y, Zs[:, :, 0], color="pink")
    ax.plot_wireframe(X, Y, Zs[:, :, 1], color="skyblue")
    ax.plot_wireframe(X, Y, Zs[:, :, 2], color="orange")
    ax.plot_wireframe(X, Y, Zs[:, :, 3], color="red")
    ax.plot_wireframe(X, Y, Zs[:, :, 4], color="green")
    ax.plot_wireframe(X, Y, Zs[:, :, 5], color="black")
    ax.set_xlabel("vertical")
    ax.set_ylabel("horizontal")
    plt.show()
    '''
    pass
