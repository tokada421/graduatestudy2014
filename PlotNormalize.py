'''
Created on 2014/10/14

@author: TakahiroOkada
'''
import ProjectValue
import os
from scipy.stats.vonmises_cython import np
from ProjectValue import FACE_PICTURE_DIRECTORY, NORMALIZED_DIRECTORY, \
    VERSION_FOLDER

def plot_normalize():
    files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY) if os.path.isfile(FACE_PICTURE_DIRECTORY + x) and x.endswith(".dat")]
    
    count = 0
    for a_file in files:
        # input
        raw_data = np.loadtxt(FACE_PICTURE_DIRECTORY + a_file, delimiter=",")
        
        # filtering
        if (0 <= ProjectValue.VERSION <= 2):
            plot_data = np.array([data for i, data in enumerate(raw_data) if i in ProjectValue.USE_POINT_LIST])
        
        # normalize
        sub_vec = np.array([np.min(plot_data.T[0]), np.min(plot_data.T[1])])
        div_vec = np.array([np.max(plot_data.T[0]) - sub_vec[0], np.max(plot_data.T[1]) - sub_vec[1]])
        normalized_data = ((plot_data.T - sub_vec[:, None]) / div_vec[:, None]).T
        
        # output
        file_name = "{0:0>3}.{1}.ndat".format(count, a_file)
        np.savetxt(NORMALIZED_DIRECTORY + VERSION_FOLDER + file_name, normalized_data, delimiter=",")
        print file_name + " is finished."
        count += 1
    pass

'''
import ProjectValue
import os
from scipy.stats.vonmises_cython import np

def plot_normalize():
    FACE_PICTURE_DIRECTORY = ProjectValue.FACE_PICTURE_DIRECTORY
    
    files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY) if os.path.isfile(FACE_PICTURE_DIRECTORY + x) and x.endswith(".dat")]
    
    cnt = 0
    for a_file in files:
        with open(FACE_PICTURE_DIRECTORY + a_file, "r") as f:
            plot_data = []
            f.readline()  # header line
            
            line = f.readline()
            while line:
                _, x , y = line.split(',')
                plot_data.append([float(x), float(y)])
                line = f.readline()
            pass
        
        np_plot_data = np.array(plot_data)
        sub_vec = np.array([np.min(np_plot_data.T[0]), np.min(np_plot_data.T[1])])
        div_vec = np.array([np.max(np_plot_data.T[0]) - sub_vec[0], np.max(np_plot_data.T[1]) - sub_vec[1]])
        
        normalized_data = ((np_plot_data.T - sub_vec[:, None]) / div_vec[:, None]).T
        
        with open(FACE_PICTURE_DIRECTORY + "normalized\\" + str(cnt) + ".ndat", "w") as f:
            try:
                [f.write("{0:>20},{1:>20}\n".format(x[0], x[1])) for x in normalized_data]
            except IndexError:
                print a_file
            pass
        cnt += 1
        pass
    
    pass
'''
