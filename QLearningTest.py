'''
Created on 2014/12/05

@author: TakahiroOkada
see http://d.hatena.ne.jp/Kshi_Kshi/20111227/1324993576
'''
import sys
import numpy as np
from __builtin__ import str
import CommonLibrary

class Square(object):
    def __init__(self, state, actions, reward):
        self.state = state
        self.actions = actions
        self.reward = reward
        pass

class QLearning(object):
    def __init__(self, reward_table):
        state_size = reward_table.shape
        field_buf = []
        for i in range(state_size[0]):
            line = []
            for j in range(state_size[1]):
                around_map = [(i, j - 1), (i, j + 1), (i - 1, j), (i + 1, j)]
                line.append(Square((i, j), [(x, y) for x, y in around_map if CommonLibrary.check_range(x, y, state_size)], reward_table[i, j]))
            field_buf.append(line)
        self.field = np.array(field_buf)
        self.q_value_table = {}
        pass
    
    def learn(self, n_count, alpha, gamma, epsilon):
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        for _ in range(n_count):
            self.learn_one_episode()
    
    def learn_one_episode(self):
        current_square = self.field[0, 0]
        while True:
            if(self.epsilon < np.random.random()):
                action = current_square.actions[np.random.choice(len(current_square.actions))]
            else:
                action = self.choose_action(current_square)
            if self.update_q_value(current_square.state, action):
                break
            else:
                current_square = self.field[action[0], action[1]]
        pass
    
    def update_q_value(self, state, action):
        current_square = self.field[state[0], state[1]]
        next_square = self.field[action[0], action[1]]
        
        q_sa = self.get_q_value(state, action)
        q_sa_max = max([self.get_q_value(action, next_action) for next_action in next_square.actions])
        r_sa = current_square.reward
        q_value = q_sa + self.alpha * (r_sa + self.gamma * q_sa_max - q_sa)
        
        self.set_q_value(state, action, q_value)
        return r_sa != 0.0
    
    def choose_action(self, square):
        best_actions = []
        q_max = -sys.maxint - 1
        for a in square.actions:
            q_value = self.get_q_value(square.state, a)
            if (q_value > q_max):
                best_actions = [a]
                q_max = q_value
            elif(abs(q_value - q_max) < np.finfo(np.float).eps):
                best_actions.append(a)
            pass
        return best_actions[np.random.choice(len(best_actions))]
    
    def get_q_value(self, state, action):
        try:
            return self.q_value_table[state][action]
        except KeyError:
            return 0.0
    
    def set_q_value(self, state, action, q_value):
        self.q_value_table.setdefault(state, {})
        self.q_value_table[state][action] = q_value
    
    def routing(self, goal_position):
        print "### Routing with Q value ###"
        current_square = self.field[0, 0]
        print "(0, 0) start"
        while True:
            action = self.choose_action(current_square)
            current_square = self.field[action[0], action[1]]
            print str(current_square.state)
            if(current_square.state == goal_position):
                break
        print str(goal_position) + " goal"
            
    def dump(self):
        print "### Dump of Q value ###"
        print "Q(s,a)"
        for state in self.q_value_table.keys():
            for action in self.q_value_table[state].keys():
                print "%s -> %s : %s" % (str(state), str(action), str(self.get_q_value(state, action)))
        pass