'''
Created on 2014/12/22

@author: TakahiroOkada
'''
import os
from ProjectValue import RESULT_DIRECTORY, VERSION_FOLDER
from CommonLibrary import string_to_tuple
from QLearningSom import QLearning
import numpy as np
import re

def view_q_value():
    dirs = [x for x in os.listdir(RESULT_DIRECTORY + VERSION_FOLDER) if os.path.isdir(RESULT_DIRECTORY + VERSION_FOLDER + x)]
    
    for n, name in enumerate(dirs):
        print n, ":", name
    
    print "select data:" 
    try:
        dir_name = dirs[input()]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(dirs)
        return
    
    files = [y for y in os.listdir(RESULT_DIRECTORY + VERSION_FOLDER + dir_name + "\\") if y.endswith(".qval")]
    
    for n, name in enumerate(files):
        print n, ":", name
    
    print "select data:" 
    try:
        file_name = files[input()]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(files)
        return
    
    print "threshold?"
    try:
        threshold = float(input())
    except:
        print "required float value"
        return
    q_value_table = {}
    with open(RESULT_DIRECTORY + VERSION_FOLDER + dir_name + "\\" + file_name, "r") as f:
        line = f.readline()
        while line:
            elem = line.split('|')
            state = string_to_tuple(elem[0])
            action = string_to_tuple(elem[1])
            q_value = float(elem[2])
            q_value_table.setdefault(state, {})
            q_value_table[state][action] = q_value
            line = f.readline()
        pass
    
    ss = re.split("\(|x|_|\)", dir_name)
    
    size0 = int(ss[1])
    size1 = int(ss[2])
    dim = int(ss[3])
    tmp = np.zeros((size0, size1, dim))
    sql = QLearning(tmp, None)
    sql.q_value_table = q_value_table
    sql.dump_q_value_with_threshold(threshold)
