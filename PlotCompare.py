'''
Created on 2014/10/14

@author: TakahiroOkada
'''
import ProjectValue
import os
import cv2

def plot_compare():
    FACE_PICTURE_DIRECTORY = ProjectValue.FACE_PICTURE_DIRECTORY
    
    files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY) if not x.endswith(".dat")]
    
    for n, name in enumerate(files):
        print n, ":", name
    
    print "select picture:"  
    try:
        file_name = files[input()]
    except NameError:
        print "required number"
        return
    except IndexError:
        print "required 0 to", len(files)
        return
    
    img = cv2.imread(FACE_PICTURE_DIRECTORY + file_name)
    
    plot_data = []
    
    with open(FACE_PICTURE_DIRECTORY + file_name + ".dat", "r") as f:
        width, height, _ = f.readline().split(',')  # header line
        
        line = f.readline()
        while line:
            index, x , y = line.split(',')
            plot_data.append([int(index), float(x), float(y)]) #float(x) * width
            line = f.readline()
    
    # Drawing
    for d in plot_data:
        cv2.circle(img, (int(d[1]), int(d[2])), 3, (0, 0, 255), -3)
    
    cv2.imshow(file_name, img)
    cv2.waitKey()
    
    cv2.destroyAllWindows()