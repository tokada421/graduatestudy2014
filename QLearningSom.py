'''
Created on 2014/12/10

@author: TakahiroOkada
'''

import numpy as np
import CommonLibrary
from numpy import indices, average
import sys
import itertools
from ProjectValue import RESULT_DIRECTORY, VERSION_FOLDER, RATE_COLUMN_NUMBER
from datetime import datetime
from __builtin__ import str
from time import time

class QLearning(object):
    def __init__(self, field, superviser):
        self.field = Field(field, superviser)
        self.q_value_table = {}
        self.superviser = superviser
        pass
    
    def learn(self, n_count, alpha, gamma, epsilon, tau):
        self.n_count = n_count
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        self.tau = tau
        start = time()
        for i in range(n_count):
            # print "Episode:", i + 1
            self.__learn_one_episode()
        end = time()
        seconds = int(end - start)
        print "finished in {0}m {1}s".format(seconds / 60, seconds % 60)
    
    def __learn_one_episode(self):
        size = self.field.get_size()
        x = np.random.choice(size[0])
        y = np.random.choice(size[1])
        current_square = self.field.get(x, y)
        while True:
            # print current_square.position
            
            if self.epsilon > np.random.random():
                action = Superviser.action_list[np.random.choice(len(Superviser.action_list))]
            else:
                action = self.__decide_action(current_square)
            
            if self.__update_q_value(current_square, action,):
                break
            else:
                if self.superviser.evaluate(current_square.position, action) < self.tau and self.epsilon > np.random.random():
                    destination = current_square.position
                else:
                    destination = self.__decide_destination(current_square)
                # destination = self.__decide_destination(current_square)
                current_square = self.field.get(destination[0], destination[1])
            pass
        pass
    
    # uniform distribution
    def __decide_destination(self, square):
        destinations = square.get_destination_candidates()
        return destinations[np.random.choice(len(destinations))]
    
    def __decide_action(self, square):
        best_actions = []
        q_max = -sys.maxint - 1
        for a in Superviser.action_list:
            q_value = self.__get_q_value(square.get_position(), a)
            if(q_value > q_max):
                best_actions = [a]
                q_max = q_value
            elif(abs(q_value - q_max) < np.finfo(np.float).eps):
                best_actions.append(a)
            pass
        
        return best_actions[np.random.choice(len(best_actions))]
    
    def __update_q_value(self, square, action):
        q_sa = self.__get_q_value(square.position, action)
        # q_sa_max = max([self.__get_q_value(destination, action) for destination in square.get_destination_candidates()])
        r_sa = self.superviser.get_initial_reward(square.position) + self.superviser.evaluate(square.position, action)
        # q_value = q_sa + self.alpha * (r_sa + self.gamma * q_sa_max - q_sa)
        q_value = q_sa + self.alpha * r_sa - self.gamma * q_sa
        
        self.__set_q_value(square.position, action, q_value)
        return r_sa > self.tau
        # return q_sa == 0
        
    def __get_q_value(self, state, action):
        try:
            return self.q_value_table[state][action]
        except KeyError:
            return 0.0
        pass
    
    def __set_q_value(self, state, action, q_value):
        self.q_value_table.setdefault(state, {})
        self.q_value_table[state][action] = q_value
                
    def dump_q_value(self):
        print "### Dump of Q value"
        for state in self.q_value_table.keys():
            for action in self.q_value_table[state].keys():
                print str(state), str(action), str(self.__get_q_value(state, action))
            pass
        pass
    
    def dump_q_value_with_threshold(self, threshold):
        print "### Dump of Q value with over", threshold
        for state in self.q_value_table.keys():
            for action in self.q_value_table[state].keys():
                q_value = self.__get_q_value(state, action)
                if(q_value > threshold):
                    print str(state), str(action), str(q_value), ":"
                    field = self.field.get(state[0], state[1])
                    neighbor_num_threshold = 1# (len(field.get_destination_candidates()) + 1) / 4
                    '''
                    print "Good" if neighbor_num_threshold <= len([x for x in field.get_destination_candidates() if self.__get_q_value(x, action) > threshold]) else "Bad"
                    '''
                    if neighbor_num_threshold <= len([x for x in field.get_destination_candidates() if self.__get_q_value(x, action) > threshold * threshold]):
                        for neighber_position in field.get_destination_candidates():
                            print "\t", str(neighber_position), self.__get_q_value(neighber_position, action)
            pass
        pass
    
    def dump_q_value_to_file(self, som_name):
        dt = datetime.now().strftime('%Y%m%d-%H%M%S')
        filename = "{0}\\{1}({2}_{3}_{4}_{5}_{6}).qval".format(som_name, dt, str(self.n_count), str(self.alpha), str(self.gamma), str(self.epsilon), str(self.tau))
        print "Dumping of Q value to", filename.split('\\')[1]
        with open(RESULT_DIRECTORY + VERSION_FOLDER + filename, "w") as f:  
            for i in range(self.field.get_size()[0]):
                for j in range(self.field.get_size()[1]):
                    state = (i, j)
                    for action in Superviser.action_list:
                        qvalue = self.__get_q_value(state, action)
                        row = "{0}|{1}|{2}".format(str(state), str(action), str(qvalue))
                        f.write(row + "\n")
                    pass
                pass
            pass

class Superviser(object):
    def __init__(self, values_in_grid, epsilon):
        self.epsilon = epsilon
        answers = []
        for i in range(values_in_grid.shape[0]):
            answers_inner = []
            for j in range(values_in_grid.shape[1]):
                value = values_in_grid[i, j][0]
                true_action = [1 if value[x] > Superviser.threshold[x] else 0 for x in range(RATE_COLUMN_NUMBER)]
                answers_inner.append(true_action)
            answers.append(answers_inner)
        self.answers = np.array(answers)
    
    def get_initial_reward(self, indices):
        # value = self.values_in_grid[indices[0], indices[1]][0]
        return 0  # 1 if value[0] > 3.0 else 0
    
    def evaluate(self, indices, action):
        if self.epsilon > np.random.random():
            return np.random.random()
        else:
            true_action = self.answers[indices[0], indices[1]]
            reward = len([x for x in range(RATE_COLUMN_NUMBER) if true_action[x] == action[x]]) / float(RATE_COLUMN_NUMBER)
            # print "true:", tuple(true_action), "action:", action, "reward:", reward
            return reward
    
    action_list = [p for p in itertools.product([0, 1], repeat=6)]
    
    threshold = [4.0, 3.5, 4.0, 4.0, 4.0, 3.0]

class Field(object):
    def __init__(self, array3d, superviser):
        self.size = array3d.shape
        field_buf = []
        for i in range(array3d.shape[0]):
            line = []
            for j in range(array3d.shape[1]):
                indices = (i, j)
                vector = array3d[i, j]
                square = Square(indices, self.__calc_transition_candidates(indices), vector, 0);
                line.append(square)
            field_buf.append(line)
        self.value = np.array(field_buf)
        pass
        
    def __calc_transition_candidates(self, indices):
        i = indices[0]
        j = indices[1]
        around_map = [(i - 1, j - 1), (i - 1, j), (i - 1, j + 1), (i, j - 1), (i, j + 1), (i + 1, j - 1), (i + 1, j), (i + 1, j + 1)]
        return [(x, y) for x, y in around_map if CommonLibrary.check_range(x, y, self.size)]
    
    def get(self, index1, index2):
        return self.value[index1, index2]
    
    def get_size(self):
        return self.size

class Square(object):
    def __init__(self, position, action_candidate, vector, initial_reward):
        self.position = position
        self.action_candidates = action_candidate
        self.value = vector
        self.initial_reward = initial_reward
        pass
    
    def get_destination_candidates(self):
        return self.action_candidates
    
    def get_position(self):
        return self.position
    
    def get_initial_reward(self):
        return self.initial_reward
