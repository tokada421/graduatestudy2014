'''
Created on 2014/10/16

@author: TakahiroOkada
'''
import numpy as np
import cv2
import os
from ProjectValue import FACE_PICTURE_DIRECTORY

def plot_all(size=600):
    img = np.zeros((size, size, 3), np.uint8)
    
    
    files = [x for x in os.listdir(FACE_PICTURE_DIRECTORY + "normalized\\")]
    for a_file in files:
        with open(FACE_PICTURE_DIRECTORY + "normalized\\" + a_file, "r") as f:
            for line in f.readlines():
                x, y = line.split(',')
                cv2.circle(img, (int(float(x) * size), int(float(y) * size)), 3, (0, 0, 255), -3)
                pass
            pass
        pass
    
    cv2.imshow("Plot All" , img)
    cv2.waitKey(0)
